const AllowedRoles = ['RAdmin', 'user', 'Expert']

module.exports = function(req,res,next) {
  if (AllowedRoles.indexOf(req.session.role) > -1) {
      next();
  } else {
      res.redirect('home');
  }
};
