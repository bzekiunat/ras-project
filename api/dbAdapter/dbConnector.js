const mysql = require('mysql');

const getDBConnection = () => {
  const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'dbbJP+2020',
    database: 'ras',
    multipleStatements: true
  });
  
  connection.connect();
  return connection;
};

// connection.query('SELECT 1 + 1 AS solution', function (err, rows, fields) {
//   if (err) throw err

//   console.log('The solution is: ', rows[0].solution)
// })

// connection.end()

module.exports = getDBConnection;