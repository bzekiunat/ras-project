const docx = require("docx");
const fs = require('fs');
const { create } = require("domain");
const { Document, Packer, Paragraph, TextRun, HeadingLevel, UnderlineType } = docx;

// Documents contain sections, you can have multiple sections per document, go here to learn more about sections
// This simple example will only contain one section

const requiredFields = [];

const createTitle = (title) => {
  return new Paragraph({
    style: 'reportDefaultParagraph',
    children: [new TextRun({
      text: title,
      underline: {
        type: UnderlineType.SINGLE,
        color: "990011",
      },
    })]
  });
};

const createHeading = (heading) => {
  return new Paragraph({
    text: heading,
    style: 'CustomHeading2',
    heading: HeadingLevel.HEADING_2
  });
};

const createParagraph = (text) => {
  return new Paragraph({
    style: 'reportDefaultParagraph',
    text
  });
};
const createParagraphList = (textList = []) => {
  return textList.map(createParagraph);
}

function generateDOCX({ answers }) {
  const getAnswer = (fieldId, defaultValue = '...') => {
    if (fieldId && answers[fieldId]) {
      if (answers[fieldId] === 'OTHER') {
        return getAnswer(fieldId + '_other', defaultValue);
      } else {
        return answers[fieldId];
      }
    }
    return defaultValue;
  };
  const getUpperCaseAnswer = (fieldId, defaultValue = '...') => {
    return getAnswer(fieldId, defaultValue).toLocaleUpperCase('tr-TR');
  };
  const getAnswerAsList = (fieldId) => {
    if (fieldId && answers[fieldId]) {
      if (typeof answers[fieldId] === 'string') {
        return [answers[fieldId]];
      }
      return answers[fieldId];
    }
    return [];
  };

  const getTadilatParagraph = () => {
    if (answers.tadilat && answers.tadilat.length > 0) {
      const tadilatTextList = answers.tadilat.map(subId => `${getAnswer(subId + '-tadilatRuhsatTarihi')} tarih ${getAnswer(subId + '-tadilatRuhsatYevmiye')} sayılı ${getAnswer(subId + '-yapilanTadilatinAdi')} tadilatı amacıyla verilmiş`);
      return new Paragraph({
        style: 'reportDefaultParagraph',
        text: `${getAnswer('tapuSicilMudurlugu')} Belediyesi İmar Müdürlüğü İmar Arşivinde yapılan dosya incelemesinde taşınmaza ait, ${tadilatTextList.join(', ')} Tadilat Yapı ${answers.tadilat.length > 1 ? 'Ruhsatları' : 'Ruhsatı'} incelenmiştir.`  
      })
    }
    return new Paragraph({
      style: 'reportDefaultParagraph',
      text: `${getAnswer('tapuSicilMudurlugu')} Belediyesi İmar Müdürlüğü İmar Arşivinde yapılan dosya incelemesinde taşınmaza ait tadilat bulunmamaktadır.`  
    })
  }; 

  const doc = new Document({
    styles: {
      paragraphStyles: [{
        id: "reportDefaultParagraph",
        name: "reportDefaultParagraph",
        basedOn: "Normal",
        next: "Normal",
        quickFormat: true,
        run: {
          size: 24,
          font: 'calibri',
          color: '000000'
        },
        paragraph: {
          size: 24,
          font: 'calibri',
          color: '000000'
        },
      }, {
        id: "CustomHeading2",
        name: "Custom Heading 2",
        basedOn: "Heading2",
        next: "Normal",
        quickFormat: true,
        run: {
          size: 24,
          font: 'calibri',
          bold: true,
          color: 'ff0000'
        },
      },]
    }
  });

  const olumluListesi = createParagraphList();

  const generatePromise = new Promise(function (resolve) {
    doc.addSection({
      properties: {},
      children: [
        new Paragraph({
          text: "1. GAYRIMENKULÜN ADRESI / KONUMU",
          style: 'CustomHeading2',
          heading: HeadingLevel.HEADING_2
        }),
        new Paragraph({
          style: 'reportDefaultParagraph',
          children: [
            new TextRun('İZMİR İLİ ' + getUpperCaseAnswer('tapuSicilMudurlugu') + ' İLÇESİ ' + getUpperCaseAnswer('tapuMahalle') + ' MAHALLESİ ' + getAnswer('adaNo') + ' ADA ' + getAnswer('parselNo') + ' PARSEL ' + getUpperCaseAnswer('kat') + ' KAT ' + getUpperCaseAnswer('bagimsizBolum') + ' NOLU 1 ADET MESKEN DEĞERLEMESİ'),
          ],
        }),
        new Paragraph({
          style: 'CustomHeading2',
          text: "2. TAPU SİCİL KAYITLARI"
        }),
        new Paragraph({
          style: 'reportDefaultParagraph',
          children: [new TextRun({
            text: "TAPU AÇIKLAMASI",
            underline: {
              type: UnderlineType.SINGLE,
              color: "990011",
            },
          })]
        }),
        new Paragraph({
          style: 'reportDefaultParagraph',
          text: 'Tarafımıza ibraz edilen tapu senedi ile tarafımızca TKGM Takbis Sistemi üzerinden alınan Takbis Belgesi (Tapu Kaydı) arasında herhangi bir farklılığa rastlanılmamıştır. Değerleme konusu taşınmaza ait takyidat bilgilerine TKGM Takbis Sisteminden alınan Takbis belgesinden ulaşılmıştır. Tarafımıza tapu kütüğünün gösterilmemesi ve görevli memurdan bilgilerin şifahen öğrenilememesi nedeniyle, Takbis belgesindeki bilgilerin doğruluğu teyit edilememiştir. Belgedeki bilgilerin doğru olduğu kabulü ile rapor tanzim edilmiştir.'
        }),
        new Paragraph({
          style: 'CustomHeading2',
          text: "IMAR DURUMU BILGILERI"
        }),
        new Paragraph({
          style: 'reportDefaultParagraph',
          children: [new TextRun({
            text: "MİMARİ PROJE",
            underline: {
              type: UnderlineType.SINGLE,
              color: "990011",
            },
          })]
        }),
        new Paragraph({
          style: 'reportDefaultParagraph',
          text: `${getAnswer('tapuSicilMudurlugu')} Belediyesinde ve TKGM internet portalında ${getAnswer('mimariProjeOnayTarihi')} onay tarihli mimari proje incelenmiştir.`
        }),
        new Paragraph({
          style: 'reportDefaultParagraph',
          children: [new TextRun({
            text: "YENİ YAPI RUHSAT:",
            underline: {
              type: UnderlineType.SINGLE,
              color: "990011",
            },
          })]
        }),
        new Paragraph({
          style: 'reportDefaultParagraph',
          text: `${getAnswer('tapuSicilMudurlugu')} Belediyesi İmar Müdürlüğü İmar Arşivinde yapılan dosya incelemesinde taşınmaza ait ${getAnswer('yapiRuhsatTarihi')} tarih ve ${getAnswer('yapiRuhsatYevmiye')} sayılı tüm bağımsız bölümler için verilmiş Yeni Yapı Ruhsatı incelenmiştir.`
        }),
        new Paragraph({
          style: 'reportDefaultParagraph',
          children: [new TextRun({
            text: "TADİLAT YAPI RUHSAT:",
            underline: {
              type: UnderlineType.SINGLE,
              color: "990011",
            },
          })]
        }),
        getTadilatParagraph(),
        new Paragraph({
          style: 'reportDefaultParagraph',
          children: [new TextRun({
            text: "YAPI KULLANMA:",
            underline: {
              type: UnderlineType.SINGLE,
              color: "990011",
            },
          })]
        }),
        new Paragraph({
          style: 'reportDefaultParagraph',
          text: `${getAnswer('tapuSicilMudurlugu')} Belediyesi İmar Müdürlüğü İmar Arşivinde yapılan dosya incelemesinde taşınmaza ait ${getAnswer('yapiKullanmaTarihi')} tarih ${getAnswer('yapiKullanmaYevmiye')} sayılı tüm bağımsız bölümler için verilmiş Yapı Kullanma İzin Belgesi incelenmiştir.`
        }),
        new Paragraph({
          style: 'reportDefaultParagraph',
          text: `${getAnswer('tapuSicilMudurlugu')} Belediyesi'nde yapılan incelemelerde taşınmaza ait herhangi bir Yapı Tatil Tutanağı, Encümen Kararı, Yıkım Kararı v.b. belgelerin bulunmadığı, herhangi bir olumsuzluğun bulunmadığı sözlü bilgisi alınmıştır. `
        }),
        new Paragraph({
          style: 'reportDefaultParagraph',
          children: [new TextRun({
            text: "İMAR AÇIKLAMA",
            underline: {
              type: UnderlineType.SINGLE,
              color: "990011",
            },
          })]
        }),
        new Paragraph({
          style: 'reportDefaultParagraph',
          text: `${getAnswer('tapuSicilMudurlugu')} Belediyesi, İmar ve Şehircilik Müdürlüğü, İmar Durumu Şubesinden alınan şifahi bilgiye göre; değerleme konusu taşınmazın konumlandığı ${getAnswer('adaNo')} ada, ${getAnswer('parselNo')} parsel, ${getAnswer('imarOnayTarihi')} onay tarihli ${getAnswer('imarPlanOlcegi')} ölçekli Uygulama İmar Planında, ${getAnswer('imarLejandi')} alanında kalmakta olup, ${getAnswer('nizam')} nizam ${getAnswer('imarKat')} kat, Taks: ${getAnswer('TAKS')}, Kaks: ${getAnswer('KAKS')} Hmaks: ${getAnswer('HMAKS')} m yapılaşma koşullarında olduğu öğrenilmiştir. Taşınmaz ${getAnswer('tapuSicilMudurlugu')} Belediyesi mücavir alan sınırları içerisinde yer almaktadır.`
        }),
        new Paragraph({
          style: 'reportDefaultParagraph',
          children: [new TextRun({
            text: "CEZAİ TUTANAK, ZABIT VB. :",
            underline: {
              type: UnderlineType.SINGLE,
              color: "990011",
            },
          })]
        }),
        new Paragraph({
          style: 'reportDefaultParagraph',
          text: `${getAnswer('tapuSicilMudurlugu')} Belediyesi İmar Müdürlüğünde taşınmaza ait herhangi bir olumsuz belge ve yıkım kararı bulunmadığı bilgisi şifahen öğrenilmiştir.`
        }),
        new Paragraph({
          style: 'CustomHeading2',
          text: "3. TAKYİDATLAR :"
        }),
        new Paragraph({
          style: 'reportDefaultParagraph',
          children: [new TextRun({
            text: "Takyidat Türleri: ",
            underline: {
              type: UnderlineType.SINGLE,
              color: "990011",
            },
          })]
        }),
        new Paragraph({
          style: 'reportDefaultParagraph',
          text: `TKGM Takbis sistemden alınan takyidat belgesi incelenmiş olup, konu taşınmaz üzerinde herhangi bir takyidat bulunmamaktadır.`
        }),
        new Paragraph({
          style: 'reportDefaultParagraph',
          children: [new TextRun({
            text: "Rehin Hakları Hanesi:",
            underline: {
              type: UnderlineType.SINGLE,
              color: "990011",
            },
          })]
        }),
        new Paragraph({
          style: 'reportDefaultParagraph',
          text: `TKGM Takbis sistemden alınan takyidat belgesi incelenmiş olup, konu taşınmaz üzerinde herhangi bir ipotek kaydı mevcut değildir.`
        }),
        new Paragraph({
          style: 'reportDefaultParagraph',
          text: `Türkiye Vakıflar Bankası T.A.O. tarafından ... tarih ... yevmiye no'lu 1. Dereceden, F.B.K vadeli, %... faizli ....000 TL rehin bulunmaktadır.`
        }),
        new Paragraph({
          style: 'reportDefaultParagraph',
          text: `TKGM Takbis sistemden alınan takyidat belgesi incelenmiş olup, konu taşınmaz üzerinde;`
        }),
        createTitle('Şerhler Hanesi:'),
        createParagraph('TKGM Takbis sistemden alınan takyidat belgesi incelenmiş olup, konu taşınmaz üzerinde herhangi bir şerh kaydı mevcut değildir.'),
        createTitle('Hak ve Mükellefiyetler'),
        createParagraph('TKGM Takbis sistemden alınan takyidat belgesi incelenmiş olup, konu taşınmaz üzerinde herhangi bir hak ve mükellefiyet kaydı mevcut değildir.'),
        createTitle('Beyanlar Hanesi:'),
        createParagraph('TKGM Takbis sistemden alınan takyidat belgesi incelenmiş olup, konu taşınmaz üzerinde; '),
        createParagraph('- Yönetim Planı : 07/01/2019 (15/01/2019-164)'),
        createHeading('GAYRİMENKULUN KONUMU, ULAŞIMI VE ÇEVRESEL ÖZELLİKLERİ'),
        createTitle('ULAŞIM TARİFİ:'),
        createParagraph(`Değerlemeye konu taşınmaz, İzmir ili, ${getAnswer('tapuSicilMudurlugu')} ilçesi, ${getAnswer('yerindeMahalle')} Mahallesi, ... caddesi üzerinde konumlu ... dış kapı numaralı binanın ${getAnswer('kat') === 'zemin' ? getAnswer('kat') : getAnswer('kat') + '.'} katında yer alan ${getAnswer('bagimsizBolum')} bağımsız bölüm numaralı meskendir. Değerleme konusu taşınmaza ulaşmak için İzmir den Aliağa ya girildikten sonra Hürriyet Caddesi üzerinden Bergama istikametinde yol takip edilir, karşılaşılan 2. Dönel kavşaktan sola Fatih caddesi yönünde dönülür. Bu cadde üzerinde ... km ilerlendikten sonra ... tarafta konumlu söz konusu taşınmaza ulaşılır. Taşınmaza ulaşım toplu taşıma araçları ile kolaylıkla sağlanabilmektedir. Taşınmazın yakın çevresinde konut amaçlı ... Katlı binalar, ... İlkokulu, ... Anadolu Lisesi, ... Belediyesi, ... Polis Merkezi Amirliği bulunmaktadır. Gayrimenkulün konumlandığı bölge, sosyoekonomik olarak orta gelir grubunun ikamet ettiği bir yer olup konut olarak gelişmiş bir bölgedir. Bölge yapılaşması genellikle konut, dini, eğitim yapıları seklindedir. Taşınmazın bulunduğu bölgede teknik ve sosyal altyapı hizmetleri tamamlanmış durumdadır.`),
        createHeading('4. ANA GAYRİMENKUL ÖZELLİKLERİ'),
        createParagraph(`Değerleme konusu taşınmazın bulunduğu bina; İzmir ili, ${getAnswer('tapuSicilMudurlugu')} ilçesi, ${getAnswer('tapuMahalle')} Mahallesi, ...,00 m2 yüz ölçümlü, ${getAnswer('adaNo')} ada, ${getAnswer('parselNo')} parsel üzerine ${getAnswer('nizam')} nizam, ${getAnswer('yapiTipi')} yapı tarzında inşa edilmiştir. Taşınmazın konumlu olduğu bina parselin kuzeyinde yer almaktadır. Değerleme konusu taşınmaz mimari projesine ve yerinde yapılan tespitlere göre ... kat + ... Normal kat + ... katı olmak üzere toplam ... katlıdır. Binanın zemin katında; bina girişi ve ... adet mesken, .... katlarında 1’er adet mesken , ... katında dubleks mesken olmak üzere ... adet bağımsız bölüm bulunmaktadır. `),
        createParagraph(`Bina dış cephesi ${getAnswer('anaTasinmazDisCephe')}, merdivenleri ve kat sahanlıkları ${getAnswer('anaTasinmazVeKat')} kaplama, merdiven evi duvarları yarıya kadar mermer kaplama, diğer yarısı plastik boyalıdır. Bina merdiven korkulukları ${getAnswer('anaTasinmazMerdivenKorkuluklari')} doğrama, giriş kapısı ${getAnswer('anaTasinmazGirisKapisi')}. Binaya giriş ... cephe olan ... yönünden zemin kattan sağlanmaktadır. Değerleme konusu taşınmazın yerinde çıplak gözle yapılan incelemelerde, depremle ilgili herhangi bir hasar gözlenememiştir. Herhangi bir deprem hasarı olup olmadığının veya yeni yönetmeliklere göre yeterliliğinin konunun uzmanları tarafından yapılacak tetkik ve deneylerle kesinlik kazanacağı muhakkaktır.`),
        createTitle('Taşınmazın Konum Açısından Proje ile Uygunluğu :'),
        createParagraph(`Değerlemeye konu olan taşınmazın ada /parsel bazında konumu imar paftasından, bina içerisindeki konumu ise ${getAnswer('tapuSicilMudurlugu')} Tapu Müdürlüğü internet portalında mevcut olan ${getAnswer('mimariProjeOnayTarihi')} onay tarihli kat irtifakına esas olan projesinden teyit edilmiş taşınmazın kat, kattaki konum bakımından projesine uygun şekilde inşa edildiği tespit edilmiştir.`),
        createTitle('Taşınmazın Büyüklük (Alan) Açısından Proje ile Uygunluğu :'),
        createParagraph(`Yerinde yapılan incelemelerde taşınmazın alan bakımından projesine uygun olarak inşa edildiği görülmüştür.`),
        createHeading('5. GAYRİMENKULUN İÇ MEKAN ÖZELLİKLERİ'),
        createParagraph(`Değerlemeye konu taşınmaz bina girişine göre ${getAnswer('BBBinaGiriseGoreKonum')} cephede ... caddesi üzerinden bakıldığında ${getAnswer('kat')}. katta yer almaktadır. Konu taşınmaz mimari projesine göre; salon, mutfak, ... oda, giriş holü koridor, banyo, wc, kömürlük, ... adet balkon hacimlerinden oluşmakta olup brüt ${getAnswer('BBAlan')} m2 kullanım alanına sahiptir.`),
        createParagraph(`İç mekân özellikleri ise; Daire kapısı ${getAnswer('BBDaireKapısı')}, ${getAnswer('BBZemin')}, duvarlar ${getAnswer('BBDuvar')}, tavanlar ${getAnswer('BBTavan')}, pencereler ${getAnswer('BBPencere')}, mutfak zemini ${getAnswer('mutfakZemin')}, duvarları ıslak hacimlerde fayans, mutfak alt ve üst dolapları ${getAnswer('mutfakDolap')}, mutfak tezgahı ${getAnswer('mutfakTezgah')}, ıslak hacimler zeminler seramik, duvarlar fayans, kapıları panel kapı seklinde olup içerisinde lavabo, klozet, duş kabini vb. vitrifiye elemanları mevcuttur. Taşınmaz ikinci kattan kısmi deniz manzarasına sahiptir, bakımlı ve kullanışlı durumdadır. Taşınmazın ısınması ${getAnswer('BBIsinma')} ile sağlanmaktadır.`),
        createTitle('Varsa: '),
        createParagraph(`Taşınmazın yerinde yapılan incelemelerde birden çok değişiklik yapıldığı tespit edilmiştir. Bu değişiklikler 1. Katta; Mutfağa bağlanan 2 balkondan yan cepheye bakan balkon mutfağa dâhil edilmiş, Yatak odasında bulunan balkonun arka cepheye bakan tarafı yatak odasına dâhil edilmiş, yan cephedeki balkon değiştirilmemiş, kömürlük ile yanındaki balkon birleştirilip yeni bir oda oluşturulmuştur. Çatı katında ise mimari projede oda olarak görünen bölümde bir adet banyo oluşturulmuş, binanın ön cephesi ve yan cephesinde bulunan çatının iç tarafı meskene dâhil edilip 2 adet oda oluşturulmuştur. 3. Katta yapılan değişiklikler herhangi bir alan artısına neden olmamakla beraber küçük tadilatlarla projesine uygun haline getirilebilir düzeydedir. Çatı katında yapılan banyonun alan artısına neden olmamasına rağmen, çatı hacminin meskene dâhil edilmesi sonucu meskende büyüme oluşmuştur. Bahsi geçen büyüme ile ilgili herhangi bir tadilat projesi bulunmamaktadır.`),
        createTitle('Yoksa: '),
        createParagraph('Değerleme konusu taşınmazın mimari projesine uygun inşa edildiği tespit edilmiştir.'),
        createHeading('DEĞERE ETKİ EDEN FAKTÖRLER'),
        createTitle('Olumlu Faktörler'),
        ...createParagraphList(getAnswerAsList('olumluOzellikleri')),
        createTitle('Olumsuz Faktörler'),
        ...createParagraphList(getAnswerAsList('olumsuzOzellikleri')),
        createHeading('DEGERLEME GENEL BILGILER'),
        createTitle('Raporu İmzalamaya Yetkili Kişinin Sonuç Cümlesi'),
        createParagraph('Konu gayrimenkulün değerlemesinde emsal karşılaştırma yöntemi kullanılmıştır. Konumuz taşınmazın değerlendirmesinde civardaki alım satım rayiç değerleri ve günümüz ekonomik koşulları, taşınmazın konumu, yası, fiziki özellikleri, emsallerdeki pazarlık payları, arz/talep dengesi gibi dışsal etkenler dikkate alınmıştır. Işbu rapor, tarihi itibariyle muteberdir.'),
        createParagraph('Yapılan tüm araştırmalar sonucunda olumlu ve olumsuz tüm faktörler dikkate alındığında, taşınmazın konumu, ulaşım durumu ve mahallindeki kullanım sekli göz önüne alınarak taşınmazın satış kabiliyeti “Satılabilir” olarak nitelendirilmiştir. '),
        createParagraph('** Taşınmazın aylık ... TL/ay bedelle kiralanabileceği değerlendirilmiştir.'),
        createParagraph('** Taşınmazın Y2235ADE12716 numaralı Enerji Kimlik Belgesi görülmüş ve sistem üzerinden sorgulanarak doğruluğu tespit edilmiştir.'),
        createTitle('Büyüme Varsa: '),
        createParagraph('... katında yapılan büyüme düzeltilebilir nitelikte olduğundan mevcut durum değerinde göz önünde bulundurulmamıştır. Taşınmazın mevcut hali ile bilgi amaçlı değerinin ....000 TL olacağı kanaatine varılmıştır. Bu değer bilgi amaçlı takdir edilmiş olup herhangi bir bağlayıcılığı bulunmamaktadır.'),
        createHeading('9. ÇEVRE FIYAT ARASTIRMASI SONUÇLARI - EMSAL DEGERLENDIRME TABLOSU'),
        createTitle('Emsal Cümlesi:'),
        createParagraph('Değerlemeye konu taşınmaz aynı bölgede, ... yıllık binada, ...katlı binanın ....katında, ...+1 hacimli, ... m2 olarak pazarlanan (... m2 olduğu düşünülen) mesken ....000 TL bedelle satılıktır. Pazarlıkla ....000 TL’ye alınabileceği düşünülmektedir.'),
        createTitle('Emsaller ile İlgili Diğer Açıklamalar'),
        createParagraph('Emsaller konum olarak aynı bölgede yer almaktadır. Emsallerin pazarlanırken belirtilen alanlarının; merdiven kovası, hava boşluğu vb alanların da eklenmesi sureti ile olağandan %15-25 daha büyük beyan edilerek söylendiği düşünülmektedir. Tabloda belirtilen emsal alanları, pazarlanan alanlar üzerinden tahmin edilen proje alanlarına indirgenmiş olarak belirtilmiştir. Yakın çevredeki emlak pazarlama firmaları ile yapılan görüşmeler ve yukarıda detayları verilen emsal bilgilerinden yola çıkılarak taşınmazın diğer olumlu/olumsuz özellikleri (manzara, iç mekan kalitesi, büyüklük ,vb) dikkate alınarak değeri takdir edilmiştir. Çevre emsalleri incelendiğinde değerleme konusu taşınmaz ile benzer özelliklerde olan meskenlerin m2 birim değerlerinin 2.000-2.100.-TL aralığında olduğu kanaatine varılmıştır.')
      ]
    });

    // Used to export the file into a .docx file
    Packer.toBuffer(doc).then((buffer) => {
      fs.writeFileSync("My Documentx.docx", buffer);
      // buffer.clearImmediate();
      resolve();
    });
  });
  return generatePromise;
}

module.exports = generateDOCX;
