var createError = require('http-errors');
var express = require('express');
var path = require('path');
const exphbs = require('express-handlebars');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const bodyParser = require('body-parser');
// var logger = require('morgan');
var cors = require("cors");
var indexRouter = require('./routes/index');
var reportRouter = require('./routes/report');
var loginRouter = require('./routes/login');
var docxAPIRouter = require("./routes/docxAPI");
var pdfParserRouter = require("./routes/pdfParser");
var userRouter = require("./routes/user");
const userAuth = require('./middlewares/user-auth');

var app = express();
// app.use(logger('dev'));

app.use(express.json());
app.use(cors());
// app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(session({
  secret: 'real-secret',
  resave: false,
  saveUninitialized: false
}));

app.engine('hbs', exphbs({
  extname: '.hbs'
}));

app.set('view engine', 'hbs');
app.use('/ras', userAuth, indexRouter); // react app index

// API calls
app.use('/report', userAuth, reportRouter);
app.use("/getReportDocx", docxAPIRouter);
app.use("/takbispdf", pdfParserRouter);
app.use("/user", userAuth, userRouter);

app.use('/static', express.static(path.join(__dirname, 'public/build/static'))); // React app static files

app.use('/login', loginRouter);
app.use('/logout', function (req, res) {
  req.session.destroy();
  res.redirect('/home');
});
app.use('/', function (req, res) {
  res.render('home');
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  console.log('err: ', err);
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
});

module.exports = app;
