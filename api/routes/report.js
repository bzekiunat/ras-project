const express = require('express');
const uniqid = require('uniqid');
const moment = require('moment');
const getDBConnection = require('../dbAdapter/dbConnector');
const router = express.Router();

router.get('/create', function(req, res, next) {
  const conn = getDBConnection();
  const { userId } = req.session;
  const id = uniqid();
  const createDate = moment().format('YYYY-MM-DD hh:mm:ss');
  
  conn.query(`INSERT INTO ras.reports SET ?`, {id, user_id: userId, create_date: createDate}, function (err, rows, fields) {
    conn.end();
  });
  res.send(id);
});

router.post('/edit', function(req, res, next) {
  const conn = getDBConnection();
  const { reportID, editData } = req.body;
  
  conn.query(`UPDATE ras.reports SET ? WHERE (id = ?)`, [editData, reportID], function (err, rows, fields) {
    conn.end();
    res.send('done');
  });
});

router.delete('/delete', function(req, res, next) {
  const conn = getDBConnection();
  const { reportID } = req.body;
  
  conn.query(`DELETE FROM ras.reports WHERE (id = ?)`, [reportID], function (err, rows, fields) {
    conn.end();
    res.send('done');
  });
});

router.post('/setAnswer', function(req, res, next) {
  const conn = getDBConnection();
  const { userId } = req.session;
  const { reportID, key, value } = req.body;
  const id = `${reportID}_${key}`;

  const queryStr = 'INSERT INTO ras.answers VALUES (?, ?, ?, ?, ?, ?)' +
  ' ON DUPLICATE KEY UPDATE value_json = ?;';

  conn.query(queryStr, [reportID, key, value, userId, '', id, value], function (err, rows, fields) {
    conn.end();
    res.send('done');
  });
});

router.get('/getAnswers/:reportID', function(req, res, next) {
  const conn = getDBConnection();
  const { userId } = req.session;
  conn.query(`SELECT * FROM ras.answers where (user_id = ? and report_id = ?);`, [userId, req.params.reportID ], function (err, rows, fields) {
    const answers = rows.reduce((allAnswers, row) => {
      allAnswers[row.answer_key] = row.value_json;
      return allAnswers;
    }, {});
    conn.end();
    res.send(answers);
  });
});

router.get('/', function(req, res, next) {
  const conn = getDBConnection();
  const { userId } = req.session;

  conn.query(`SELECT id, create_date, bank_id, estate_type, name FROM ras.reports where (user_id = ?);`, [userId], function (err, rows, fields) {
    const reportsList = [];
    const reportDetails = {};
    rows.forEach(row => {
      reportDetails[row.id] = {
        createDate: row.create_date,
        bankID: row.bank_id,
        reportName: row.name,
        estateType: row.estate_type
      };
      reportsList.push(row.id);
    });
    conn.end();
    res.send({ reportsList, reportDetails });
  });
});

module.exports = router;
