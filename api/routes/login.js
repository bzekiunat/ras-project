
var express = require('express');
var path = require('path');
const getDBConnection = require('../dbAdapter/dbConnector');
var router = express.Router();

router.get('/', (req, res, next) => {
  res.render('login');
});

router.post('/', (req, res, next) => {
  const { username, password } = req.body;
  const connection = getDBConnection();
  connection.query(`SELECT * FROM ras.user where (user_name = ?);`, [username], function (err, rows, fields) {
    if (err) {
      res.render('login', {
        message: 'Giriş yapılamadı',
        messageClass: 'alert-danger'
      });
      connection.end();
      throw err;
    }
    if (rows[0] && rows[0].password === `${password}`) {
      req.session.role = rows[0].role;
      req.session.username = username;
      req.session.userId = rows[0].id;
      res.redirect('/ras');
    } else {
      res.render('login', {
        message: 'Yanlış kullanıcı adı ya da şifre',
        messageClass: 'alert-danger'
      });
    }
    connection.end();
  });
});

module.exports = router;