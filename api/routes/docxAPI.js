var express = require("express");
var generateDOCX = require("../public/javascripts/docxGenerator");
var router = express.Router();

router.post("/", function(req, res, next) {
    var generatingPromise = generateDOCX(req.body.reportData);
    var options = {
      root: __dirname,
      headers: {
        'x-timestamp': Date.now(),
        'x-sent': true
      }
    }
    generatingPromise.then(function() {
      res.download('./My Documentx.docx', 'My Documentx.docx', function (err) {
        if (err) {
          next(err);
        }
      });
    }).catch(function(err) {
      next(err);
    });
    // res.send("docx file created");
});

module.exports = router;