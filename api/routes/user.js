var express = require('express');
var uniqid = require('uniqid');
var router = express.Router();

router.get('/', function(req, res, next) {
  if (req.session.userId) {
    res.send({
      id: req.session.userId,
      role: req.session.role
    });
  }
});

module.exports = router;
