const fs = require('fs');
const express = require('express');
const router = express.Router();
const multer  = require('multer');
const pdfreader = require('pdfreader');
const getDBConnection = require('../dbAdapter/dbConnector');
const storage = multer.memoryStorage();
const upload = multer({ storage: storage });

let rows = {};

router.post('/', upload.single('takbispdf'), function(req, res, next) {
  const conn = getDBConnection();
  const allRows = [];
  const  {reportID } = req.body;
  new pdfreader.PdfReader().parseBuffer(req.file.buffer, function(err, item) {
    if (!item) {
      const answers = {};
      allRows.forEach((row, index) => {
        const search = ' / '; 
        const replacer = new RegExp(search, 'g');
        const replaced = row.replace(replacer, '/');
        if (replaced.indexOf('Parsel:') > -1) {
          const raw = replaced.split('Parsel:')[1];
          const [adaNo, parselNo] = raw.split('/');
          answers.adaNo = adaNo;
          answers.parselNo = parselNo;
        } else if (replaced.indexOf('İlçe:') > -1){
          const raw = replaced.split('İlçe:')[1].split('Bağımsız Bölüm Nitelik:')[0];
          const [Il, tapuSicilMudurlugu] = raw.split('/');
          answers.tapuSicilMudurlugu = tapuSicilMudurlugu;
        } else if (replaced.indexOf('Blok/Kat/Giriş/BBNo:') > -1) {
          const raw = replaced.split('Blok/Kat/Giriş/BBNo:')[1];
          const [blok, kat, giris, bagimsizBolum] = raw.split('/');
          answers.kat = kat;
          answers.bagimsizBolum = bagimsizBolum;
          answers.blok = blok;
        } else if (replaced.indexOf('Mahalle/Köy Adı:') > -1) {
          const raw = replaced.split('Mahalle/Köy Adı:')[1];
          const [tapuMahalle] = raw.split('/');
          answers.tapuMahalle = tapuMahalle;
        }
      });

      let query = "";
      Object.keys(answers).forEach(answerKey => {
        const { userId } = req.session;
        // [reportID, key, value, userId, '', id, value]
        query += `INSERT INTO ras.answers VALUES ('${reportID}', '${answerKey}', '${answers[answerKey]}', '${userId}', '', '${reportID}_${answerKey}')` +
        ` ON DUPLICATE KEY UPDATE value_json = '${answers[answerKey]}'; `;
      });

      conn.query(query, function (err, dbRows, fields) {
        conn.end();
      });

      res.send(answers);
    } else if (item.page) {
      // end of file, or page
      const currentRow = Object.keys(rows)
      .sort((y1, y2) => parseFloat(y1) - parseFloat(y2))
      .forEach(y => {
        allRows.push((rows[y] || []).join(""));
      });
      rows = {}; // clear rows for next page
    } else if (item.text) {
      // accumulate text items into rows object, per line
      (rows[item.y] = rows[item.y] || []).push(item.text);
    }
  });
});

module.exports = router;
