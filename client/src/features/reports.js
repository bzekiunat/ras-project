import { createSlice } from '@reduxjs/toolkit';

export const reportSlice = createSlice({
  name: 'reports',
  initialState: {
    reportDetails: {},
    myReportsList: []
  },
  reducers: {
    setActiveReportID: (state, action) => {
      const { reportID } = action.payload;
      state.activeReportID = reportID;
    },
    setAnswer: (state, action) => {
      const { reportID, fieldID, answer } = action.payload;
      if (state.reportDetails[reportID]) 
      state.reportDetails[reportID].answers[fieldID] = answer;
    },
    setAnswers: (state, action) => {
      const { data, reportID } = action.payload;
      if (state.reportDetails[reportID]){
        state.reportDetails[reportID].answers = { ...state.reportDetails[reportID].answers, ...data };
      } else {
        state.reportDetails[reportID] = { answers: data };
      }
    },
    createReport: (state, action) => {
      const { reportID, bankID, estateType, createDate } = action.payload;
      state.reportDetails[reportID] = {
        createDate,
        bankID,
        estateType,
        answers: {} 
      };
      state.myReportsList.push(reportID);
    },
    deleteReport:(state, action) => {
      const { reportID } = action.payload;
      delete state.reportDetails[reportID];
      const reportIndex = state.myReportsList.indexOf(reportID);
      if (reportIndex > -1) {
        state.myReportsList.splice(reportIndex, 1);
      }
    },
    editReport: (state, action) => {
      const { reportID, editData } = action.payload;
      state.reportDetails[reportID] = {
        ...state.reportDetails[reportID],
        ...editData
      };
    },
    setMyReportsList: (state, action) => {
      const { payload: { reportsList, reportDetails = {} } } = action;
      const myReportsList = reportsList || [];
      state.myReportsList = myReportsList;
      myReportsList.forEach((rid) => {
        state.reportDetails[rid] = {
          ...reportDetails[rid],
          answers: {}
        };
      });
    }
  }
});

export const { setActiveReportID, setAnswer, setAnswers, createReport, setMyReportsList, editReport, deleteReport } = reportSlice.actions;

// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state) => state.counter.value)`
// export const getActiveRepo = state => state.counter.value;

export default reportSlice.reducer;
