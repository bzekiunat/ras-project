// BANK NAMES
export const ISBANK = 'ISBANK';
export const VAKIFBANK = 'VAKIFBANK';
export const YAPIKREDI = 'YAPIKREDI';

export const mudurlukler = ["", "Aliağa", "Balçova", "Bayındır", "Bayraklı", "Bergama", "Beydağ", "Bornova", "Buca", "Çeşme", "Çiğli", "Dikili", "Foça", "Gaziemir", "Güzelbahçe", "Karabağlar", "Karaburun", "Karşıyaka", "Kemalpaşa", "Kiraz", "Kınık", "Konak", "Menderes", "Menemen", "Narlıdere", "Ödemiş", "Seferihisar", "Selçuk", "Tire", "Torbalı", "Urla"];

export const isBankEvForm = [{
  label: 'Ada No',
  controlId: 'adaNo',
  type: 'number'
}, {
  label: 'Parsel No',
  controlId: 'parselNo',
  type: 'number'
}, {
  label: 'Tapu Mahalle',
  controlId: 'tapuMahalle',
  type: 'text'
}, {
  label: 'Yerinde Mahalle',
  controlId: 'yerindeMahalle',
  type: 'text'
}, {
  label: 'Kat',
  controlId: 'kat',
  type: 'text'
}, {
  label: 'Bağımsız Bölüm Numara',
  controlId: 'bagimsizBolum',
  type: 'number'
}, {
  label: 'Tapu Sicil Müdürlüğü',
  controlId: 'tapuSicilMudurlugu',
  type: 'dropdown',
  options: mudurlukler
}, {
  label: 'Mimari Proje Onay Tarihi',
  controlId: 'mimariProjeOnayTarihi',
  type: 'date'
}, {
  label: 'Yapı Ruhsat Tarihi',
  controlId: 'yapiRuhsatTarihi',
  type: 'date'
}, {
  label: 'Yapı Ruhsat Yevmiye',
  controlId: 'yapiRuhsatYevmiye',
  type: 'text'
}, {
  label: 'Tadilat',
  controlId: 'tadilat',
  type: 'subFormsContainer',
  subFormElementList: [{
    label: 'Yapılan Tadilatın Adı',
    controlId: 'yapilanTadilatinAdi',
    type: 'text'
  }, {
    label: 'Tadilat Ruhsat Tarihi',
    controlId: 'tadilatRuhsatTarihi',
    type: 'date'
  }, {
    label: 'Tadilat Ruhsat Yevmiye',
    controlId: 'tadilatRuhsatYevmiye',
    type: 'text'
  }]
}, {
  label: 'Yapı Kullanma Var mı?',
  controlId: 'yapiKullanmaVarMi',
  type: 'dropdown',
  options: ["", "Evet", "Hayır"]
}, {
  label: 'Yapı Kullanma Tarihi',
  controlId: 'yapiKullanmaTarihi',
  type: 'date'
}, {
  label: 'Yapı Kullanma Yevmiye',
  controlId: 'yapiKullanmaYevmiye',
  type: 'text'
}, {
  label: 'Yapı Kullanma Bağımsız Bölüm',
  controlId: 'yapiKullanmaBagimsizBolum',
  type: 'text'
}, {
  label: 'İmar Plan Ölçeği',
  controlId: 'imarPlanOlcegi',
  type: 'dropdown',
  options: ["", "1/1000","1/5000","1/25000","1/100000"]
}, {
  label: 'İmar Lejandı',
  controlId: 'imarLejandi',
  type: 'dropdown',
  options: ["", "konut","ticaret","sanayi","konut dışı kentsel çalışma", 'tarım', 'akaryakıt istasyonu', "turizm", "eğitim", "sağlık", "merkezi iş", "sosyal donatı alanı(park, yeşil alan, ağaçlandırılacak alan, belediye hizmet alanı, dini tesis alanı)"]
}, {
  label: 'İmar Onay Tarihi',
  controlId: 'imarOnayTarihi',
  type: 'date'
}, {
  label: 'TAKS',
  controlId: 'TAKS',
  type: 'number'
}, {
  label: 'KAKS',
  controlId: 'KAKS',
  type: 'number'
}, {
  label: 'HMAKS',
  controlId: 'HMAKS',
  type: 'number'
}, {
  label: 'Nizam',
  controlId: 'nizam',
  type: 'text'
}, {
  label: 'Yapı cinsi',
  controlId: 'yapiTipi',
  type: 'dropdown',
  options: ["", "Betonarme","Kargir/Yığma","Ahşap","Çelik"]
}, {
  label: 'İmar Kat',
  controlId: 'imarKat',
  type: 'number'
}, {
  label: 'Ana Taşınmaz Dış Cephe Durumu',
  controlId: 'anaTasinmazDisCephe',
  type: 'dropdown',
  options: ["", "Dış cephe boyalı","Kompozit kaplama","Cam kaplama"]
}, {
  label: 'Ana Taşınmaz Merdiven ve Kat Kaplama',
  controlId: 'anaTasinmazVeKat',
  type: 'dropdown',
  options: ["", "Mermer","Granit","Dökme Taş","Beton"]
}, {
  label: 'Ana Taşınmaz Duvarlar',
  controlId: 'anaTasinmazDuvarlar',
  type: 'dropdown',
  options: ["", "Plastik boyalı","Saten boyalı","Yarıya kadar mermer kaplama, diğer yarısı plastik boyalı"]
}, {
  label: 'Ana Taşınmaz Merdiven Korkulukları',
  controlId: 'anaTasinmazMerdivenKorkuluklari',
  type: 'dropdown',
  options: ["", "alüminyum","demir","ferforje"]
}, {
  label: 'Ana Taşınmaz Giriş Kapısı',
  controlId: 'anaTasinmazGirisKapisi',
  type: 'dropdown',
  options: ["", "Camlı demir","Demir","Ferforje","Alüminyum","Çelik kapı"]
}, {
  label: 'BB\'nin Bina Girişine Göre Konumu',
  controlId: 'BBBinaGiriseGoreKonum',
  type: 'dropdown',
  options: ["", "Sol ön ve yan","Sağ ön ve yan","Sol arka ve yan","Sağ arka ve yan","Ön","Arka"]
}, {
  label: "BB'nin Alanı",
  controlId: 'BBAlan',
  type: 'number'
}, {
  label: "BB'nin Daire Kapısı",
  controlId: 'BBDaireKapısı',
  type: 'dropdown',
  options: ["", "çelik kapı","ahşap kapı","demir kapı","alüminyum kapı"]
}, {
  label: "BB'nin Zeminleri",
  controlId: 'BBZemin',
  type: 'dropdown',
  options: ["", "Salon, odalar ve hol zeminleri parke","Salon ve oda zeminleri parke, hol alanının zemini fayans","Salon zemini parke, oda ve hol alanının zeminleri fayans","Salon, odalar ve hol zeminleri fayans"]
}, {
  label: "BB'nin Duvarları",
  controlId: 'BBDuvar',
  type: 'dropdown',
  options: ["", "Plastik boyalı","Saten boyalı","Duvar kağıdı kaplı"]
}, {
  label: "BB'nin Tavanları",
  controlId: 'BBTavan',
  type: 'dropdown',
  options: ["", "Kartonpiyer","Alçı","Plastik boyalı"]
}, {
  label: "BB'nin Pencereleri",
  controlId: 'BBPencere',
  type: 'dropdown',
  options: ["", "PVC","Alüminyum Doğrama","Demir doğrama","Ahşap"]
}, {
  label: "Mutfak Zemini Özellikleri",
  controlId: 'mutfakZemin',
  type: 'dropdown',
  options: ["", "Seramik","Fayans","Parke","Ahşap"]
}, {
  label: "Mutfak Alt ve Üst Dolapları",
  controlId: 'mutfakDolap',
  type: 'dropdown',
  options: ["", "MDF","MDF-Lam","Akrilik","Ahşap","Sunta"]
}, {
  label: "Mutfak Tezgahı Özellikleri",
  controlId: 'mutfakTezgah',
  type: 'dropdown',
  options: ["", "Mermer","Granit","Mermerit","Çimstone"]
}, {
  label: "BB'nin Isınma Özellikleri",
  controlId: 'BBIsinma',
  type: 'dropdown',
  options: ["", "Doğalgaz","Merkezi ısıtma","Soba","Klima","Yerden Isıtma","Jeotermal"]
}, {
  label: "Gayrimenkulün Olumlu Özellikleri",
  controlId: 'olumluOzellikleri',
  type: 'checkbox',
  options: ["Merkeze yakın konumda olması","Ara katta olması","Yeni binada olması","Tercih edilen bölgede olması","Tercih edilen cephede olması","Deniz manzaralı olması","Şehir manzaralı olması","Park manzaralı olması","Orman manzaralı olması","Köşe parselde bulunması","Ulaşımın kolay olması","Sosyal imkanlara yakın olması","Site içerisinde olması","İçinin yapılı olması"]
}, {
  label: "Gayrimenkulün Olumsuz Özellikleri",
  controlId: 'olumsuzOzellikleri',
  type: 'checkbox',
  options: ["Eski binada olması","Ulaşımın zor olması","İskanının olmaması","Projeye aykırı imalatların bulunması","Merkeze kısmen uzak olması","Manzarasının olmaması","Karanlık odasının olması","Tadilata ihtiyacının olması","Gayrimenkul sektöründeki durgunluk"]
}];

const prod = {
  baseURl: '/ras',
  apiURl: ''
};
 const dev = {
  baseURl: '/ras',
  apiURl: 'http://localhost:9000'
 };
 export const config = process.env.NODE_ENV === 'development' ? dev : prod;


// {
//   label: 'Tadilat Bağımsız Bölüm Numarası',
//   controlId: 'tadilatBagimsizBolumNo',
//   type: 'text'
// }, {
//   label: 'İmar Alanı Türü',
//   controlId: 'imarAlaniTürü',
//   type: 'dropdown',
//   options: ["", "Konut","Tarım","Konut + Ticaret","Sanayi"]
// }, {
//   label: 'Ana Taşınmaz Kat Sayısı',
//   controlId: 'anaTasinmazKatSayisi',
//   type: 'dropdown',
//   options: ["", "3. Bodrum","2. Bodrum","1. Bodrum","Zemin","1.","2.","3.","4.","5.","6.","7.","8.","9.","10.","11.","12."]
// }, {
//   label: 'Ana Taşınmaz Toplam BB',
//   controlId: 'anaTasinmazToplamBB',
//   type: 'number'
// }, {
//   label: "BB'nin Oda Sayısı",
//   controlId: 'BBOdaSayisi',
//   type: 'number'
// }, {
//   label: "BB'nin İç Bölümleri",
//   controlId: 'BBIcBolumler',
//   type: 'dropdown',
//   options: ["", "salon","mutfak","giriş holü","koridor","banyo","2 adet banyo","3 adet banyo","Wc","2 adet wc","Kömürlük","Balkon","2 adet balkon","3 adet balkon","4 adet balkon","Teras","2 adet teras","Ebeveyn banyosu"]
// },