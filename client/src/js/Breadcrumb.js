import React from 'react';
import { Breadcrumb, Container } from 'react-bootstrap';
import { useLocation } from 'react-router-dom';

const breadcrumbList = [{
  link: '/bankaSecimi',
  text: 'Banka Seçimi'
}, {
  link: '/emlakTipi',
  text: 'Emlak Tipi'
}, {
  link: '/dataGirisi',
  text: 'Data Girşi'
}];

function BreadcrumbComp() {
  const location  = useLocation();
  const rid = location.pathname.split('/')[2];
  

  let stepNumber = 0;
  breadcrumbList.forEach((breadcrumbItem, index) => {
    if (location.pathname === `/ras/${rid}${breadcrumbItem.link}`) stepNumber = index + 1;
  });

  const filteredList = breadcrumbList.slice(0, stepNumber);
  return filteredList.length > 0 && (
    <Container>
      <Breadcrumb>
        {filteredList.map((breadcrumbItem, index) => {
          const conf = stepNumber === index + 1 ? {active: true} : {};
          return (
            <Breadcrumb.Item {...conf} href={`/ras/${rid}${breadcrumbItem.link}`}>
              {breadcrumbItem.text}
            </Breadcrumb.Item>
          );
        })}
        {/* {stepNumber === 3  && <Breadcrumb.Item href="" active>Tapu</Breadcrumb.Item>} */}
      </Breadcrumb>
    </Container>
  );
}

export default BreadcrumbComp;