import isEvRG from "./isbank/evRG";

export function generatePDF(reportData) {
  if (reportData.bankID === 'isBank') {
    if (reportData.estateType === 'ev') {
      
      const report = new isEvRG(reportData);
      report.createPDF();
      
    }
  }
}

export function generateDOCX(reportData) {
  const report = new isEvRG(reportData);
  report.createPDF();
}