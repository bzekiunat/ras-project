import React from 'react';
import { Navbar, Nav, Container } from 'react-bootstrap';
import logo from '../images/ras-logo.jpeg';
import axios from 'axios';
import { useHistory } from 'react-router-dom';
import { createReport } from '../features/reports';
import { useDispatch } from 'react-redux';
import { config } from './constants';

// const uniqid = require('uniqid');
// import '../style/App.css';

function NavigationBar() {
  let history = useHistory();
  const dispatch = useDispatch();
  const handleCreateReport = (e) => {
    axios.get(config.apiURl + '/report/create').then(function (response) {
      const rid = response.data;
      history.push(`${config.baseURl}/${response.data}/bankaSecimi`);

      let myReportsList = localStorage.getItem('myReportsList') || '[]';
      myReportsList = JSON.parse(myReportsList);
      myReportsList.push(rid);
      const createDate = new Date();
      const bankID = 'vakifBank';
      const estateType = 'mesken';
      localStorage.setItem('myReportsList', JSON.stringify(myReportsList));
      localStorage.setItem(`${rid}_answers`, '{}');
      localStorage.setItem(`${rid}_conf`, JSON.stringify({
        createDate: createDate.toString(),
        bankID,
        estateType,
      }));
      dispatch(createReport({ reportID: rid, createDate: createDate.toString(), bankID, estateType }));
    }).catch(function (error) {
      console.log(error);
    });
  };

  return (
    <div className="navigationBar-container bg-light">
      <Container >
        <Navbar bg="light" expand="lg">
          <Navbar.Brand href="/ras">
            <img
              src={logo}
              width="60"
              height="60"
              className="d-inline-block align-top"
              alt="RAS logo"
            />
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
              <Nav.Link href="/ras">Ana Sayfa</Nav.Link>
              <Nav.Link onClick={handleCreateReport}>Rapor Oluştur</Nav.Link>
            </Nav>
            <Nav>
            <Nav.Link href="/logout">Çıkış</Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </Container>
    </div>

  );
}

export default NavigationBar;