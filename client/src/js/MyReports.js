import React from 'react';
import { Container, Table } from 'react-bootstrap';
import { useSelector } from 'react-redux';
import TableRow from './TableRow';

function MyReports() {
  const { myReportsList, reportDetails } = useSelector(state => state.reports);
  return (
    <Container>
      <div className="myReports-title">RAPORLARIM</div>
      <Table striped bordered hover responsive>
        <thead>
          <tr>
            <th>Rapor Adı</th>
            <th>Banka</th>
            <th>Emlak Tipi</th>
            <th>Tarih</th>
            <th>Sil</th>
            <th>#</th>
          </tr>
        </thead>
        <tbody>
          {myReportsList.map(rid => <TableRow rid={rid} details={reportDetails[rid]} />)}
        </tbody>
      </Table>
    </Container>
  );
}

export default MyReports;