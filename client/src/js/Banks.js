import React from 'react';
import { Container, Col, Row, Image } from 'react-bootstrap';
import { useDispatch, } from 'react-redux';
import vakifbank from '../images/vakifbank.png';
import isbank from '../images/isbank.png';
import yapikredi from '../images/yapikredi.svg';
import { VAKIFBANK, ISBANK, YAPIKREDI, config } from './constants';
import { useHistory, useParams } from 'react-router-dom';
import axios from 'axios';
import { editReport } from '../features/reports';

function Banks() {
  const history = useHistory();
  const params = useParams();
  const dispatch = useDispatch();

  const handleBankChoice = (bankID) => () => {
    // TODO: trim string answers
    history.push(`${config.baseURl}/${params.rid}/emlakTipi`);
    const editData = {
      bank_id: bankID
    };
    axios({
      method: 'post',
      url: `${config.apiURl}/report/edit`,
      data: {
        reportID: params.rid,
        editData
      }
    }).then(function (response) {
      dispatch(editReport( { reportID: params.rid, editData: { bankID } }));
    }).catch(function (error) {
      console.log(error);
    });
  }

  return (
    <Container>
      <Row>
        <Col xs={6} md={4}>
          <button className="bank-thumbnail-button" onClick={handleBankChoice(VAKIFBANK)}>
            <Image src={vakifbank} thumbnail />
            <div>VakıfBank</div>
          </button>
        </Col>
        <Col xs={6} md={4}>
          <button disabled className="bank-thumbnail-button" onClick={handleBankChoice(ISBANK)}>
            <Image style={{backgroundColor: '#013682'}} src={isbank} thumbnail  />
            <div>Türkiye İş Bankası</div>
          </button>
        </Col>
        <Col xs={6} md={4}>
          <button disabled className="bank-thumbnail-button" onClick={handleBankChoice(YAPIKREDI)}>
            <Image style={{backgroundColor: '#1366b2'}} src={yapikredi} thumbnail />
            <div>YapıKredi</div>
          </button>
        </Col>
      </Row>
    </Container>
  );
}

export default Banks;