import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector, connect } from 'react-redux';
import { Form, Container, Col, Row, Button } from 'react-bootstrap';
import { useParams, withRouter } from "react-router-dom";
import Textarea from './fields/Textarea';
import NumberBox from './fields/NumberBox';
import TextBox from './fields/TextBox';
import Dropdown from './fields/Dropdown';
import Checkbox from './fields/Checkbox';
import DateField from './fields/DateField';
import SubFormsContainer from './fields/SubFormsContainer';
import debounce from 'lodash.debounce';
import axios from 'axios';

import { setAnswer, setAnswers, editReport } from '../features/reports';
// import { generatePDF } from './reportGenerators';
import { isBankEvForm, config } from './constants';


const fieldMap = {
  dropdown: Dropdown,
  checkbox: Checkbox,
  text: TextBox,
  textarea: Textarea,
  number: NumberBox,
  date: DateField,
  subFormsContainer: SubFormsContainer
};

class GenericForm extends React.Component {
  constructor(props) {
    super(props);
    this.debouncerSetAnswer = debounce(this.setAnswerApiCall.bind(this),200);
    this.debouncerEditReport = debounce(this.editReportApiCall.bind(this),200);
  }
  componentDidMount() {
    const { rid } = this.props;
    axios.get(config.apiURl + '/report/getAnswers/' + rid).then((response) => {
      this.props.setAnswers({data: response.data, reportID: rid});
    });
  }

  setAnswerApiCall = (value, key, rid) => {
    axios({
      method: 'post',
      url: `${config.apiURl}/report/setAnswer`,
      data: {
        reportID: rid,
        key,
        value
      }
    }).then(function (response) {
    }).catch(function (error) {
      console.log(error);
    });
  };

  editReportApiCall = (value, rid) => {
    const editData = {
      name: value
    };
    axios({
      method: 'post',
      url: `${config.apiURl}/report/edit`,
      data: {
        reportID:rid,
        editData
      }
    }).then(function (response) {
    }).catch(function (error) {
      console.log(error);
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    const { reportData, rid } = this.props;
    // TODO: validate fields
    axios({
      method: 'post',
      url: `${config.apiURl}/getReportDocx`,
      responseType: 'blob',
      data: { reportData }
    }).then(function (response) {
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement('a');
      link.href = url;
      const reportName  = reportData.reportName || rid;
      link.setAttribute('download', `rapor.docx`); //or any other extension
      document.body.appendChild(link);
      link.click();
    }).catch(function (error) {
      console.log(error);
    });
  }

  handleEditReport = ({ value }) => {
    // TODO: trim string answers
    const { rid } = this.props;
    this.props.editReport( { reportID: rid, editData: { reportName: value } });
    this.debouncerEditReport(value, rid);
  }

  handleChange = ({ value, key }) => {
    // TODO: trim string answers
    const { rid } = this.props;
    this.props.setAnswer( { reportID: rid, fieldID: key, answer: value });
    this.debouncerSetAnswer(value, key, rid);
  }

  onFileChange = (event) => {
    const { rid } = this.props;
    const formData = new FormData(); 
    // Update the formData object 
    const file = event.target.files[0];
    formData.append(
      "takbispdf",
      file
    );
    formData.append(
      "reportID",
      rid
    );
    const conf = {
      headers: {
        'content-type': 'multipart/form-data'
      }
    }
    axios.post(`${config.apiURl}/takbispdf`, formData, conf).then((response) => {
      this.props.setAnswers({data: response.data, reportID: rid});
    });
  };

  render() {
    const { reportData } = this.props;
    const answers  = reportData.answers || {};
    const reportName  = reportData.reportName || '';

    return (
      <Container>
        <div>
          <Form onSubmit={this.handleSubmit}>
            <Form.Group as={Row} controlId="takbisPDF">
              <Form.Label column sm={2}>Web Tapu Takbis PDF</Form.Label>
              <Col sm={10}>
                <Form.File
                  label=""
                  name="takbispdf"
                  data-browse="Gözat"
                  onChange={this.onFileChange}
                  custom
                />
              </Col>
            </Form.Group>
            <TextBox controlId="reportName" handleChange={this.handleEditReport} value={reportName} label= "Rapor Adı" />
            {isBankEvForm.map(({ type, ...fieldConf }) => {
              const Field = fieldMap[type];
              return <Field {...fieldConf} handleChange={this.handleChange} value={answers[fieldConf.controlId]} otherValue={answers[fieldConf.controlId + '_other']} />;
            })}
            <Form.Group as={Row}>
              <Col sm={{ span: 10, offset: 2 }}>
                <Button type="submit">Rapor Oluştur</Button>
              </Col>
            </Form.Group>
          </Form>
        </div>
      </Container>
    );
  }
}

const mapStoreToProps = (store, ownProps) => {
  const rid = ownProps.match.params.rid;
  const reportData = store.reports.reportDetails[rid] || {};
  return {
    rid,
    reportData
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setAnswer: (data) => dispatch(setAnswer(data)),
    setAnswers: (data) => dispatch(setAnswers(data)),
    editReport: (data) => dispatch(editReport(data)),
  }
}

export default withRouter(connect(mapStoreToProps, mapDispatchToProps)(GenericForm));

// function FormTree() {
//   const dispatch = useDispatch();
//   const params = useParams();
//   const reportData = useSelector(state => state.reports.reportDetails[params.rid]) || {};
//   const answers  = reportData.answers || {};
//   const reportName  = reportData.reportName || '';

//   const debouncedReportName = useDebounce(reportName, 200);
//   const debouncedAnswers = useDebounce(answers, 200);

//   useEffect(() => {
//     editReportApiCall(reportName, params.rid);
//   }, [debouncedReportName]);

//   useEffect(() => {
//     setAnswerApiCall(value, key, params.rid);
//   }, [debouncedAnswers]);

//   useEffect(() => {
//     axios.get(config.apiURl + '/report/getAnswers/' + params.rid).then(function (response) {
//       dispatch(setAnswers);
//     });
//   }, []);

// }
