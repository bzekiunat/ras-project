import React, { useEffect } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import NavigationBar from './NavigationBar';
import Breadcrumb from './Breadcrumb';
import FormTree from './FormTree';
import ReportPreview from './ReportPreview';
import Banks from './Banks';
import SideBar from './SideBar';
import MyReports from './MyReports';
import RealEstateTypes from './RealEstateTypes';
import '../style/App.css';
import { setMyReportsList } from '../features/reports';
import { useDispatch } from 'react-redux';
import axios from 'axios';
import { config } from './constants';

function App() {
  const dispatch = useDispatch();
  useEffect(() => {
    axios.get(config.apiURl + '/report/').then(function (response) {
      // const { reportsList, reportDetails } = response.data;
      dispatch(setMyReportsList(response.data));
    });
    
  }, []);
  return (
    <Router>
      <div className="App">
        <NavigationBar />
        {/* <Switch>
          <Route path="/:rid"> */}
            <div className="app-body">
              <Breadcrumb />
              <Switch>
                <Route path="/ras/raporOnizleme" children={<ReportPreview />} />
                <Route path="/ras/:rid/dataGirisi">
                  <div className="app-body-form">
                    {/* <SideBar /> */}
                    <FormTree />
                  </div>
                </Route>
                <Route path="/ras/:rid/bankaSecimi" children={<Banks />} />
                <Route path="/ras/:rid/emlakTipi" children={<RealEstateTypes />} />
                <Route path="/ras/">
                  <MyReports />
                </Route>
              </Switch>
            </div>
          {/* </Route> */}
        {/* </Switch> */}
      </div>
    </Router>
  );
}

export default App;