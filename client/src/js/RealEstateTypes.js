import React from 'react';
import { Container, Col, Row, Image } from 'react-bootstrap';
import house from '../images/house.jpg';
import { ISBANK, YAPIKREDI, config } from './constants';
import { useHistory, useParams } from 'react-router-dom';
import axios from 'axios';
import { useDispatch, } from 'react-redux';

import { editReport } from '../features/reports';

function RealEstateTypes() {
  const history = useHistory();
  const params = useParams();
  const dispatch = useDispatch();

  const handleBankChoice = (estateType) => () => {
    // TODO: trim string answers
    history.push(`${config.baseURl}/${params.rid}/dataGirisi`);
    const editData = {
      estate_type: estateType
    };
    axios({
      method: 'post',
      url: `${config.apiURl}/report/edit`,
      data: {
        reportID: params.rid,
        editData
      }
    }).then(function (response) {
      dispatch(editReport( { reportID: params.rid, editData: { estateType } }));
    }).catch(function (error) {
      console.log(error);
    });
  }


  return (
    <Container>
      <Row>
        <Col xs={6} md={4}>
          <button className="bank-thumbnail-button" onClick={handleBankChoice('Mesken')}>
            <Image src={house} thumbnail />
            <div>Mesken</div>
          </button>
        </Col>
        <Col xs={6} md={4}>
          <button disabled className="bank-thumbnail-button" onClick={handleBankChoice(ISBANK)}>
            <Image src={house} thumbnail  />
            <div>İş Yeri</div>
          </button>
        </Col>
        <Col xs={6} md={4}>
          <button disabled className="bank-thumbnail-button" onClick={handleBankChoice(YAPIKREDI)}>
            <Image src={house} thumbnail />
            <div>Arsa</div>
          </button>
        </Col>

        <Col xs={6} md={4}>
          <button disabled className="bank-thumbnail-button" onClick={handleBankChoice(YAPIKREDI)}>
            <Image src={house} thumbnail />
            <div>Bina</div>
          </button>
        </Col>
        <Col xs={6} md={4}>
          <button disabled className="bank-thumbnail-button" onClick={handleBankChoice(YAPIKREDI)}>
            <Image src={house} thumbnail />
            <div>Diğer</div>
          </button>
        </Col>
      </Row>
    </Container>
  );
}

export default RealEstateTypes;