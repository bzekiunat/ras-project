import React from 'react';

function SideBar() {
  return (
    <div className="sidebar">
      <button className="sidebar-item">Tapu</button>
      <button className="sidebar-item">Belediye</button>
      <button className="sidebar-item">Rapor İndir</button>
    </div>
  );
}

export default SideBar;
