import React from 'react';
import { IMaskInput } from 'react-imask';
import { Form, Col, Row } from 'react-bootstrap';

function DateField({ controlId, label, handleChange, value }) {
  const handleInputChange = (e) => {
    handleChange({ value: e.target.value, key: controlId });
  };

  return (
    <Form.Group as={Row} controlId={controlId}>
      <Form.Label column sm={2}>{label}</Form.Label>
      <Col sm={10}>
        <IMaskInput
          type="text"
          mask="00/00/0000"
          placeholder="gg/aa/yyyy"
          value={value}
          onChange={handleInputChange}
          className="form-control"
        />
      </Col>
    </Form.Group>
  );
}

export default DateField;