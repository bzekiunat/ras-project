import React from 'react';
import { Form, Col, Row } from 'react-bootstrap';

function TextBox({ controlId, label, handleChange, value }) {
  const handleInputChange = (e) => {
    handleChange({ value: e.target.value, key: controlId });
  };
  return (
    <Form.Group as={Row} controlId={controlId}>
      <Form.Label column sm={2}>{label}</Form.Label>
      <Col sm={10}>
        <Form.Control onChange={handleInputChange} value={value}/>
      </Col>
    </Form.Group>
  );
}

export default TextBox;