import React from 'react';
import { Form, Col, Row } from 'react-bootstrap';

function Dropdown({ controlId, label, options, handleChange, value, otherValue }) {
  const handleInputChange = (e) => {
    handleChange({ value: e.target.value, key: controlId });
  };
  const handleOtherInputChange = (e) => {
    handleChange({ value: e.target.value, key: controlId + '_other' });
  };
  return (
    <Form.Group as={Row} controlId={controlId}>
      <Form.Label column sm={2}>{label}</Form.Label>
      <Col sm={10}>
        <Form.Control as="select" onChange={handleInputChange} value={value}>
          {options.map(opt => <option value={opt}>{opt}</option>)}
          <option value="OTHER">Diğer</option>
        </Form.Control>
        {value === 'OTHER' && (
          <Form.Control onChange={handleOtherInputChange} value={otherValue}/>
        )}
      </Col>
    </Form.Group>
  );
}

export default Dropdown;

// adduser ertemu
// # !!! enter passwords here 

// sudo adduser ertemu sudo; # sudoers a eklemek için

// su ertemu
// cd ~;
// git clone https://ertem@bitbucket.org/teamertemu/marketwatcher.git;
// cd marketwatcher;


// ################# Ubuntu setup #################

// passwd; # change root password

// sudo apt-get install curl -y;
// curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -;
// sudo apt-get install nodejs -y;

// # puppeteer dependencies https://github.com/GoogleChrome/puppeteer/blob/master/docs/troubleshooting.md
// sudo apt-get install zip unzip python gconf-service libasound2 libatk1.0-0 libatk-bridge2.0-0 libc6 libcairo2 libcups2 libdbus-1-3 libexpat1 libfontconfig1 libgcc1 libgconf-2-4 libgdk-pixbuf2.0-0 libglib2.0-0 libgtk-3-0 libnspr4 libpango-1.0-0 libpangocairo-1.0-0 libstdc++6 libx11-6 libx11-xcb1 libxcb1 libxcomposite1 libxcursor1 libxdamage1 libxext6 libxfixes3 libxi6 libxrandr2 libxrender1 libxss1 libxtst6 ca-certificates fonts-liberation libappindicator1 libnss3 lsb-release xdg-utils wget -y;

// sudo apt-get install build-essential make gconf-service libasound2 libatk1.0-0 libatk-bridge2.0-0 libc6 libcairo2 libcups2 libdbus-1-3 libexpat1 libfontconfig1 libgcc1 libgconf-2-4 libgdk-pixbuf2.0-0 libglib2.0-0 libgtk-3-0 libnspr4 libpango-1.0-0 libpangocairo-1.0-0 libstdc++6 libx11-6 libx11-xcb1 libxcb1 libxcomposite1 libxcursor1 libxdamage1 libxext6 libxfixes3 libxi6 libxrandr2 libxrender1 libxss1 libxtst6 ca-certificates fonts-liberation libappindicator1 libnss3 lsb-release xdg-utils wget -y;


// # node-gyp hatası alırsan
// sudo apt install python2.7 python-pip -y;
