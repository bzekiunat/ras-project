import React from 'react';
import { Form, Col, Row, Dropdown, Button } from 'react-bootstrap';
import Checkbox from './Checkbox';
import TextBox from './TextBox';
import Textarea from './Textarea';
import NumberBox from './NumberBox';
import DateField from './DateField';
import { useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

function SubFormsContainer({ controlId, label, subFormElementList, handleChange, value = [] }) {
  const params = useParams();
  const reportData = useSelector(state => state.reports.reportDetails[params.rid]) || {};
  const answers  = reportData.answers || {};
  const addSubForm = () => {
    const subFormID = new Date().valueOf();
    const valueList = [...value, `${controlId}-${subFormID}`];
    handleChange({ value: valueList, key: controlId });
  };

  const removeSubForm = (e) => {
    handleChange({ value: value.filter(subFormId => subFormId !== e.target.value), key: controlId });
    // todo: remove subform's answers 
  };

  const fieldMap = {
    dropdown: Dropdown,
    checkbox: Checkbox,
    text: TextBox,
    textarea: Textarea,
    number: NumberBox,
    date: DateField
  };

  return (
    <Form.Group as={Row} controlId={controlId}>
      <Form.Label column sm={2}>{label}</Form.Label>
      <Col sm={10}>
        {value.map((subFormId, index) => {
          return (
            <div className="subFormContainer">
              <div className="subFormContainer-header"><span>{index + 1}. Tadilat</span><Button value={subFormId} variant="danger" onClick={removeSubForm}>Sil</Button></div>
              {subFormElementList.map(({ type, controlId: subControlId, ...fieldConf }) => {
                const Field = fieldMap[type];
                const subFieldId = `${subFormId}-${subControlId}`
                return <Field {...fieldConf} controlId={subFieldId} handleChange={handleChange} value={answers[subFieldId]} otherValue={answers[subFieldId + '_other']} />;
              })}
            </div>
          );
        })}
        <Button onClick={addSubForm} variant="primary">Yeni {label} Ekle</Button>
      </Col>
    </Form.Group>
  );
}

export default SubFormsContainer;
