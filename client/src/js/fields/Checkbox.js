import React from 'react';
import { Form, Col, Row } from 'react-bootstrap';

function Checkbox({ controlId, label, options, handleChange, value = [] }) {
  const handleInputChange = (e) => {  
    const allCheckboxes = document.querySelectorAll(`.${controlId} input`);
    const selectedList = [];
    for (let index = 0; index < allCheckboxes.length; index++) {
      const checkbox = allCheckboxes[index];
      if (checkbox.checked) {
        selectedList.push(options[index]);
      }
    }
    handleChange({ value: selectedList, key: controlId });
  };
  return (
    <Form.Group as={Row}>
      <Form.Label column sm={2}>{label}</Form.Label>
      <Col sm={10}>
        <Row>
          {options.map((opt, index) => <Form.Check onChange={handleInputChange} type="checkbox" checked={value.indexOf(opt) > -1} className={`${controlId} col-4 mb-2`} id={controlId + '_' + index} label={opt} />)}
        </Row>
      </Col>
    </Form.Group>
  );
}

export default Checkbox;
