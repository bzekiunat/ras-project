import React from 'react';
import { useHistory } from 'react-router-dom';
import { deleteReport } from '../features/reports';
import { config } from './constants';
import { Button } from 'react-bootstrap';
import { useDispatch } from 'react-redux';
import axios from 'axios';

function TableRow({ rid, details }) {
  // const reportData = useSelector(state => state.reports);
  const history = useHistory();
  const dispatch = useDispatch();
  const handleDeleteReport = (e) => {
    e.stopPropagation();
    axios({
      method: 'delete',
      url: `${config.apiURl}/report/delete`,
      data: { reportID: rid }
    }).then(function (response) {
      dispatch(deleteReport({reportID: rid}));
    });
  };
  return (
    <tr className="myReports-table-row" onClick={()=>(history.push(`${config.baseURl}/${rid}/dataGirisi`))}>
      <td>{details.reportName || ''}</td>
      <td>{details.bankID}</td>
      <td>{details.estateType}</td>
      <td>{details.createDate}</td>
      <td><Button variant="danger" onClick={handleDeleteReport}>Sil</Button></td>
      <td>{rid}</td>
    </tr>
  );
}

export default TableRow;