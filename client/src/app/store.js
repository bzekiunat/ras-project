import { configureStore } from '@reduxjs/toolkit';
import reportReducer from '../features/reports';

export default configureStore({
  reducer: {
    reports: reportReducer,
  },
});
